# OmniPack

OmniPack Integra los distintos medios de env&iacute;o necesitando as&iacute; hacerse una sola integraci&oacute;n para todos ellos.

## &iquest;C&oacute;mo funciona?

A continuaci&oacute;n se muestra el diagrama de secuencia correspondiente al funcionamiento de omniPack integrado en un comercio.

![Diagrama de Secuendia](http://plantuml.com/plantuml/png/fPJFKeCm4CRlFCKUzT0NO3hCQDj33nrq-WGnB1Y5XCaVPlITFFa8VJ6N5aAYLAkUWB3xVRk_RCAuLHfMjK1fCM7SYevPz0l34nOPh5I3cWi5AUonHawlai4E2reg451sXpSLA0pX2TVOV-UD57UCFoTUXpJ7HTBSIcEPv891_l24La7BD0EieJaw66zP12PUr0ijWahFMQsKB4NbIBxtQD1gPVeMcBFKhE2CYyExXEMopsaf9qqDsflnhwKghgwpxDUcnjMMmTvnfmshqKGE5TfRJG0Uyx8K7CqdX-n4UeqRsHttmEjHuSPBJcZ2y2JAZUjQO-UmxYAH0kTbmdA2SV0IKsiHXN68KjWvD5QpA71smZQrgAQIG86GbKR3Q0bQhLeQaqBeVY9eftv4pfKLhpExIipldR9uCUj0w1-aGvM_eWurvbblPQbq4zSy3_ju7lhGuGn_Tsoci8WaF_sx06JaT0d3e0YYHUEZNiwX2e_X63fpxqIODgTcXWRwssNC_28r97q0)

##&iquest;C&oacute;mo se usa?

### Instanciar el objeto Omnipack

El primer paso en la integraci&oacute;n es Instanciar el objeto Omnipack pasando c&oacute;mo parametro un string con el nombre del medio de pago seleccionado. Por el momento los medios de pago integrados son:

* Andreani
* OCA

*Ejemplo:*
```php
$omnipack = new Omnipack\Omnipack($paymentMethod);
```

### Setear par&aacute;metros de autenticaci&oacute;n &lt;&lt;setAutentication()&gt;&gt;

Para comunicarse con el medio de pago usted necesitar&aacute; configurar sus credenciales, para ello deber&aacute; llamar al m&eacute;todo _setAutentication()_ pasando las credenciales en un array. A continuaci&oacute;n se listan los parametros seg&uacute;n el medio seleccionado:
**Andreani:**
* Username
* Password
* CodigoCliente
* NumeroContrato

**Oca:**
* Cuit
* NumeroOperativa

*Ejemplo:*
```php
$omnipack->setAutentication($params)
```

### Consultar sucursales &lt;&lt;getBranchOffices()&gt;&gt;

Para que el env&iacute;o pueda hacerse a una sucursal ser&aacute; necesario consultar las sucursales de la regi&oacute;n, esto se hace mediante el m&eacute;todo _getBranchOffices()_. Si se quieren filtrar las sucursales puede pasarselerse como parametro un array conlos filtros. En caso de no pasarsele filtros devolver&aacute; todas las sucursales.

Los filtros posibles son:

Key del array | Descripci&oacute;n
--------------|-
state         |Provincia
city          |Localidad
postal_code   |C&oacute;digo postal


*Ejemplo:*
```php
$filters = array("city" =&gt; "Ciudad Autonoma de Buenos Aires" );
$branchOffices =$omnipack->getBranchOffices($filters);
```

Este m&eacute;todo devolver&aacute; un array con las sucursales en objetos del tipo _BranchOffice_

### Creaci&oacute;n de objeto Address

Si el env&iacute;o es a domicilio se necesitar&aacute; crear un objeto Address pasandole c&oacute;mo par&aacute;metros: calle, n&uacute;mero de puerta, piso, departamento, c&oacute;digo postal, localidad y provincia.

*Ejemplo:*
```php
$street = "Av. Avellaneda";
$number = 2511;
$floor = 2;
$apartment = "A";
$postalCode = "C1243VGF";
$city = "Ciudad Autonoma de Buenos Aires";
$state = "Ciudad Autonoma de Buenos Aires";

$address = new Loaction\Address($street, $number, $floor, $apartment, $postalCode, $city, $state);
```

### Creaci&oacute;n del objeto Pack

Para la estimaci&oacute;n y el env&iacute;o deber&aacute; crearse un objeto _Pack_ pasandole como par&aacute;metros: altura, ancho, profundidad y peso. (En cm y en grs)

*Ejemplo:*
```php
$heigth = 20;
$width = 30;
$deep = 50;
$weight = 530;

$pack = new Pack($heigth, $width, $deep, $weight);
```
### Cotizar env&iacute;o &lt;&lt;getQuote()&gt;&gt;

Para saber cuanto costar&aacute; determinado envio antes de confirmarlo se dispone del m&eacute;todo _getQuote_. A este se le debe pasar un objeto del tipo _Pack_, con informaci&oacute;n del paquete y la ubicaci&oacute;n en un objeto que extienda de _location_, las posibilidades son _BranchOffice_ o _Address_.

*Ejemplo:*
```php
$quote = $omnipack->getQuote($pack, $location);
```

Esto devolver&aacute; un objeto del tipo _Quote_.


### Confirmar el env&iacute;o &lt;&lt;confirm()&gt;&gt;

Para realizar el env&iacute;o se dispone el m&eacute;todo _confirm()_. A este m&eacute;todo, al igual que al _getQuote_, deben pasarsele los parametros pack y location, y adicionalmente un objeto del tipo _Addressee_.

```php
$omnipack->confirm($pack, $location, $addressee);
```

## Clases de OmniPack:

### Omnipack

La clase principal es _ Omnipack_, esta es a la cu&aacute;l nos integramos. Esta clase implementa la interfaz iOmnipack y al intanciarse crea un Objeto seg&uacute;n el medio de env&iacute;o que hereda de la clase hija de _Connector_ correspondiente al protocolo utilizado por el medio de env&iacute;o en sus WebServices.

![Diagrama de Clases OmniPack](http://plantuml.com/plantuml/png/VLBBJiCm43oJhx349HLAFw0290X8FL2rFEwLSJQDXUErRATGbVuxzgRK2PMusJCxkxDZosMhfU7YWrr_JwVizvR9CRw0Ik83FEgjn1Tm_Z_-5Rd9IjGQX4SR-A9WfFc3KLqa-klPYCYVKwJAfFPWQow09IFxZB4h1_wkyw2z5DnBrFc4pTWxeeha1lozvLeqoxgM0XoHt5g-oqD3TdFBXlJaA7dkq0ELqYcY0dKjRPkQ7x9CAEuS6xafN2EDCL9l5k0Rh4BrAOCaIR54pMM38P4PSzu6NGGCsWW4X_7MER1ZElOO0gBl5vPVUza2TWErGi7LMe0TZsYyD-i6U4MzAP48WwuCncI6jQ05Lk6SG1hAbSednnTOMVpQhRZbBOHdwcFUegoEWSvfq3pCJCn5r9Kdvs-zyP4o53n5-sLmduXJZFIJzba2wF_GCtowIWrtA8IIG6l5cf8kSOiYc4i8pHWK77u0)

### Location

Se proveen 2 clases hijas de una abstracta _Location_ para la estimaci&oacute;n y la confirmaci&oacute;n del env&iacute;o. Las 2 clases disponibles son _BranchOffice_ para el caso de las sucursales y Address para el caso de los env&iacute;os a domicilio.

![Diagrama de Clases Loaction](http://plantuml.com/plantuml/png/RP31JWCX48RlFCKSDMPVm7XfQqnCMZNf4yp2R9U4XGrC3qPzTu5jjo0U-NuOlldtJY9hP-7fk-jWwz7AyNqOj2InhwADKfv240Bxm1ubWpGO0gotlWI0-15YmUMa77LzGLuLF48Se-qfHOg2z7hEOqgE-SyA1JcIEXkgu8JQL60UdIskHFHm8jvM1dVtvPEKFzyqsd3Pl-N7Yr2R7A9LIp-IMedNoYuDBst5a8YhfUnvwib7f2sdys2SAuuue-U9xEqHv2fSO3Gxf-hAfERFkbX6RYixPersYxVialdoVO8lIUWFsrojYXbmhM9LQp_OHRz_oawILNc_)

### Pack

Tambi&eacute;n se provee una clase _Pack_ para los datos del paquete a enviar.

![Diagrama de Clases Pack](http://plantuml.com/plantuml/png/Iyv9B2vM24XCpbQevb800jsCrCpqaWm8kpmp1SPCIKqjW0e25MIKS84vskcf9HvW3HgQS85ma2ualWjGAx8qM3zGe1O0)

### Quote

La estimaci&oacute;n devolver&aacute; un objeto _Quote_. A continuaci&oacute;n el diagrama de este:

![Diagrama de Clases](http://plantuml.com/plantuml/png/TSn12eD038NXlQV8SeRY1Gej21v0Zv2Q885nfin4K4hlNgDTs4Mt_tjyGy1Ie9-JChmgWFgPPM2u0gNv4TWACLAGQ56YMa4YKKaHGukAMyUSSNL5iyGHRdUOP67orVQzZAoTaSwVKSljrzsNGxRS_E3E_zNV7m00)

Notese la propiedad _additionalData_, debido a que cada medio de env&iacute;o devuelve parametros distintos, proveemos los datos espec&iacute;ficos de cada medio de env&iacute;o en un array, para el caso de querer recuperar esa informaci&oacute;n.
