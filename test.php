<?php
require_once 'vendor/autoload.php';
use Omnipack\Data as Data;

$sm = new Omnipack\Omnipack("Andreani");

echo "<pre>";
if($sm->setAutentication(array('Username'=>'STAGING_WS', 'Password' => 'ANDREANI'))) {
  echo "Autentication OK<br />";
}

echo "Sucursales:<br />";
echo "----------<br />";

print_r($sm->getBranchOffices(array('state' => 'Buenos Aires', 'postal_code' => '1406')));

$street = "Av. Avellaneda";
$number = 2511;
$floor = 2;
$apartment = "A";
$postalCode = "C1243VGF";
$city = "Flores";
$state = "Ciudad Autonoma de Buenos Aires";

$ad = new Data\Location\Address($street, $number, $floor, $apartment, $postalCode, $city, $state);

echo "<br />";
echo "Domicilio:<br />";
echo "---------";
echo "<br/>Calle: ".$ad->getStreet();
echo "<br/>Nro: ".$ad->getNumber();
echo "<br/>Piso: ".$ad->getFloor();
echo "<br/>Dto: ".$ad->getApartment();
echo "<br/>C.P.:".$ad->getPostalCode();
echo "<br/>Localidad:".$ad->getCity();
echo "<br/>Provincia".$ad->getState();

echo "<br /><br />";

echo "Paquete:<br />";
echo "-------";
$heigth = 20;
$width = 30;
$deep = 50;
$weight = 530;

$pk = new Data\Pack($heigth, $width, $deep, $weight);
echo "<br />Alto:".$pk->getHeigth();
echo "<br />Ancho:".$pk->getWidth();
echo "<br />Profundidad:".$pk->getDeep();
echo "<br />Peso:".$pk->getWeight();

echo "<br />";

echo "Cotizaci&oacute;n:<br />";
echo "----------";

$qt = $sm->getQuote($pk, $ad);
echo "<br />Precio:".$qt->getPrice();
echo "<br />Tiempo de env&iacute;o:".$qt->getDeadline();
echo "<br />:";
print_r($qt->getAdditionalData());

echo "<br />";

echo "Remitente:<br />";
echo "---------";

$name = "Alejandro";
$lastname = "Avalos";
$documentType = "DNI";
$documentNumber = "31242123";
$phone = "4121-1231";
$address = "Maipu 942";

$ae = new Data\Addressee($name, $lastname, $documentType, $documentNumber, $phone, $address);

echo "<br />name: ".$ae->getName();
echo "<br />lastname: ".$ae->getLastname();
echo "<br />documentType: ".$ae->getDocumentType();
echo "<br />documentNumber: ".$ae->getDocumentNumber();
echo "<br />phone: ".$ae->getPhone();
echo "<br />address: ".$ae->getAddress();

echo "<br /><br />";

if ($sm->confirm($pk, $ad, $ae)) {
  echo "Pedido enviado!<br />";
  echo "==============";
}

echo "</pre>";
