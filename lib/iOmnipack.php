<?php
namespace Omnipack;

interface iOmnipack {

    public function setAutentication(array $autentication_data);

    public function getBranchOffices(array $filters);

    public function getQuote(Data\Pack $pack, Data\Location\Location $location, $priority, $aditionalParams);

    public function confirm(Data\Pack $pack, Data\Location\Location $location, Data\Addressee $addressee, $priority, $aditionalParams);
}
