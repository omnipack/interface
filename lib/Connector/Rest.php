<?php
namespace Omnipack\Connector;
require_once "Connector.php";

/**
 *
 */
class Rest extends Connector
{
  const GET = "GET";
  const POST = "POST";
  // const PUT = "PUT";
  // const PATCH = "PATCH";
  // const DELETE = "DELETE";
  // const COPY = "COPY";
  // const HEAD = "HEAD";
  // const OPTIONS = "OPTIONS";
  // const LINK = "LINK";
  // const UNLINK = "UNLINK";
  // const PURGE = "PURGE";
  // const LOCK = "LOCK";
  // const UNLOCK = "UNLOCK";
  // const PROPFIND = "PROPFIND";
  // const VIEW = "VIEW";

  function __construct($endpoint, $encoding=self::JSON){
    parent::__construct($endpoint, $encoding);
  }

  public function call($service, $params, $http_method=self::GET, $additionalHeaders=array()) {
    return $this->doRest($this->endpoint.$service, $params, $http_method, $additionalHeaders);
  }

  private function doRest($url, $data = array(), $method , $headers = array()){
    if ($method == self::GET) {
      foreach ($data as $key => $value) {
        $url .= "$key/$value/";
      }
    }
    echo "$url";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

    $conn_headers = array_filter(explode("\r\n",$this->http_header));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array_merge($conn_headers, $headers));

    if($method == self::POST) {
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($data));
    }
    if($this->host != null)
      curl_setopt($curl, CURLOPT_PROXY, $this->host);
    if($this->port != null)
      curl_setopt($curl, CURLOPT_PROXYPORT, $this->port);

    $result = curl_exec($curl);
    $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    echo "sTATUS: $http_status";
    var_dump($result);
    if($http_status != 200) {
      $result = ($this->encoding == self::JSON)? "{}" : "<Colections/>";
    }
    if( json_decode($result) != null ) {
      return json_decode($result,true);
    }
    return json_decode(json_encode(simplexml_load_string($result)), true);
  }
}
