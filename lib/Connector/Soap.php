<?php
namespace Omnipack\Connector;

/**
 *
 */
class Soap extends Connector
{
  private $wsdl;
  private $client;
  private $soap_header;

  function __construct($wsdl, $endpoint, $http_header=array(), $encoding=self::XML)
  {
    parent::__construct($endpoint, $encoding, $http_header);
    $this->wsdl = $wsdl;
    $this->client = $this->getClientSoap();
  }

  public function setSoapHeader($namespace, $name, $data, $mustUnderstand=false, $actor=null )
  {
    $this->soap_header = new \SoapHeader($namespace, $name, $data, $mustUnderstand);
    $this->client = $this->getClientSoap();
  }

  private function getClientSoap(){
    $local_wsdl = $this->wsdl;
    $local_end_point = $this->endpoint;
    $context = array(
      // 'http' => array(
      //   'header'  => $this->http_header
      // ),
        'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
      )
    );
    // Fix bug #49853 - https://bugs.php.net/bug.php?id=49853
    if(version_compare(PHP_VERSION, '5.3.10') == -1) {
      $clientSoap = new Soap\Client($local_wsdl, array(
          'local_cert'=>($this->local_cert),
          'connection_timeout' => $this->connection_timeout,
          'location' => $local_end_point,
          'encoding' => 'UTF-8',
          'proxy_host' => $this->host,
          'proxy_port' => $this->port,
          'proxy_login' => $this->user,
          'proxy_password' => $this->pass
        ));
      $clientSoap->setCustomHeaders($context);
    }
    else {
      $clientSoap = new \SoapClient($local_wsdl, array(
        'stream_context' => stream_context_create($context),
        'local_cert'=>($this->local_cert),
        'connection_timeout' => $this->connection_timeout,
        'location' => $local_end_point,
        'encoding' => 'UTF-8',
        'proxy_host' => $this->host,
        'proxy_port' => $this->port,
        'proxy_login' => $this->user,
        'proxy_password' => $this->pass,
        'trace' => true,
        'soap_version'   => SOAP_1_2
      ));
    }

    if (isset($this->soap_header)){
      $clientSoap->__setSoapHeaders($this->soap_header);
    }

    return $clientSoap;
  }

  public function call($method, $params) {
    $response = $this->client->$method($params);
    // $response = $this->client->__SoapCall($method, $params, array(), array());
    return json_decode(json_encode($response), true);
  }

  public function _getLastRequest()
  {
    var_dump($this->client->__getLastRequestHeaders());
    echo "<script>var rq ='".$this->client->__getLastRequest()."'; </script>";
  }
}
