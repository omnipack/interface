<?php
namespace Omnipack\Connector;

/**
 *
 */
class Connector
{
  const JSON = "json";
  const XML = "xml";

  protected $host = NULL;
  protected $port = NULL;
  protected $user = NULL;
  protected $pass = NULL;
  protected $connection_timeout = NULL;
  protected $local_cert = NULL;
  protected $http_header=array();
  protected $endpoint;
  protected $encoding;

  function __construct($endpoint, $encoding, $http_header=array())
  {
    $this->endpoint = $endpoint;
    $this->encoding = $encoding;
    $this->http_header = $this->getHeaderHttp($http_header);
  }

  public function setProxyParameters($host = null, $port = null, $user = null, $pass = null){
		$this->host = $host;
		$this->port = $port;
		$this->user = $user;
		$this->pass = $pass;
	}

  private function getHeaderHttp($header_http_array){
		$header = "";
		if(is_array($header_http_array)) {
			foreach($header_http_array as $key=>$value){
				$header .= "$key: $value\r\n";
			}
		}
		return $header;
	}

  /**
	* Setea time out (deaulft=NULL)
	* ejemplo:
	* $todopago->setConnectionTimeout(1000);
	*/
	public function setConnectionTimeout($connection_timeout){
		$this->connection_timeout = $connection_timeout;
	}

  /**
  * Setea ruta del certificado .pem (deaulft=NULL)
  * ejemplo:
  * $omnipack->setLocalCert('c:/miscertificados/certificado.pem');
  */
  public function setLocalCert($local_cert){
    $this->local_cert= file_get_contents($local_cert);
  }
}
