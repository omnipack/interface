<?php
namespace Omnipack\Data\Location;

/**
 *
 */
class BranchOffice extends Location
{
    private $branchOfficeId;
    private $description;
    private $address;
    private $schedule;
    private $mail;
    private $phone;
    private $additionalData;

    function __construct($branchOfficeId=null, $description=null, $address=null, $schedule=null, $mail=null, $phone=null, $additionalData=null)
    {
        $this->branchOfficeId = $branchOfficeId;
        $this->description = $description;
        $this->address = $address;
        $this->schedule = $schedule;
        $this->mail = $mail;
        $this->phone = $phone;
        $this->additionalData = $additionalData;
    }

    /**
     * Get the value of Branch Office Id
     *
     * @return mixed
     */
    public function getBranchOfficeId()
    {
        return $this->branchOfficeId;
    }

    /**
     * Get the value of Description
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the value of Addres
     *
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get the value of Schedule
     *
     * @return mixed
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Get the value of Mail
     *
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Get the value of Phone
     *
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

}
