<?php
namespace Omnipack\Data\Location;

/**
 *
 */
class Address extends Location
{
    private $street;
    private $number;
    private $floor;
    private $apartment;
    private $postalCode;
    private $city;
    private $state;
    private $country;


    function __construct($street=null, $number=null, $floor=null, $apartment=null, $postalCode=null, $city=null, $state=null, $country=null )
    {
        $this->street    = $street;
        $this->number    = $number;
        $this->floor     = $floor;
        $this->apartment = $apartment;
        $this->postalCode= $postalCode;
        $this->city      = $city;
        $this->state     = $state;
        $this->country   = $country;
    }

    /**
     * Get the value of Street
     *
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Get the value of Number
     *
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get the value of Floor
     *
     * @return mixed
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Get the value of Apartment
     *
     * @return mixed
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * Get the value of Postal Code
     *
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Get the value of City
     *
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Get the value of State
     *
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Get the value of Country
     *
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

}
