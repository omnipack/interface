<?php
namespace Omnipack\Data;

/**
 *
 */
class Pack
{
    private $heigth;
    private $width;
    private $deep;
    private $weight;

    function __construct($heigth, $width, $deep, $weight)
    {
        $this->heigth = $heigth;
        $this->width = $width;
        $this->deep = $deep;
        $this->weight = $weight;
    }

    /**
     * Get the value of Heigth
     *
     * @return mixed
     */
    public function getHeigth()
    {
        return $this->heigth;
    }

    /**
     * Get the value of Width
     *
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Get the value of Deep
     *
     * @return mixed
     */
    public function getDeep()
    {
        return $this->deep;
    }

    /**
     * Get the value of Weight
     *
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

}
