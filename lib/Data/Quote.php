<?php
namespace Omnipack\Data;

/**
 *
 */
class Quote
{
  private $price;
  private $deadline;
  private $additionalData;

  function __construct($price, $deadline, $additionalData)
  {
    $this->price = $price;
    $this->deadline = $deadline;
    $this->additionalData = $additionalData;
  }

    /**
     * Get the value of Price
     *
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get the value of Deadline
     *
     * @return mixed
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Get the value of Additional Data
     *
     * @return mixed
     */
    public function getAdditionalData()
    {
        return $this->additionalData;
    }

}
