<?php
namespace Omnipack\Data;

/**
 *
 */
class Addressee
{
  private $name;
  private $lastname;
  private $documentType;
  private $documentNumber;
  private $phone;
  private $address;

  function __construct($name, $lastname, $documentType, $documentNumber, $phone, $address)
  {
    $this->name = $name;
    $this->lastname = $lastname;
    $this->documentType = $documentType;
    $this->documentNumber = $documentNumber;
    $this->phone = $phone;
    $this->address = $address;
  }


    /**
     * Get the value of Name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the value of Lastname
     *
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Get the value of Document Type
     *
     * @return mixed
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * Get the value of Document Number
     *
     * @return mixed
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * Get the value of Phone
     *
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the value of Address
     *
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

}
