<?php
namespace Omnipack;

/**
 *
 */
class Omnipack implements iOmnipack
{
    private $shipppingMethod;

    function __construct($shipppingMethod)
    {
        $class = "Omnipack\ShippingMethod\\$shipppingMethod";
        // require_once 'lib/ShippingMethod/Mock.php';
        $this->shipppingMethod = new $class();
    }

    //Autenticación
    public function setAutentication(array $params){
        $this->shipppingMethod->setAutentication($params);
    }

    public function getBranchOffices(array $filters=array()) {
        return $this->shipppingMethod->getBranchOffices($filters);
    }

    public function getQuote(Data\Pack $pack, Data\Location\Location $location, $priority=null, $additionalParams=null) {
        return $this->shipppingMethod->getQuote($pack, $location, $priority, $additionalParams);
    }

    public function confirm(Data\Pack $pack, Data\Location\Location $location, Data\Addressee $addresse, $priority=null, $additionalParams=null) {
        return $this->shipppingMethod->confirm($pack,$location, $addresse, $priority, $additionalParams);
    }
}
