<?php
namespace OmniPack\ShippingMethod; 
require_once __DIR__ . "/../Data/Location/BranchOffice.php";
require_once __DIR__ . "/../Data/Location/Address.php";

class Oca implements iOmniPack extends \OmniPack\Client\Soap
{
	protected $ws;


	public function getBranchOffices(array $params=null)
	{	
		$branchOffices = array();
	    if ( is_array($params) ){

	    	$jsonResponse = '{
	    		"0":{ "Descripcion":"Agente Oficial GARCÍA CLAUDIO",
					"Direccion":"La Rioja 301",
					"HoradeTrabajo":"lu a vi 9 - 18hs",
					"Sucursal":"Agente Oficial",
					"Telefono1":"4981-7449"
				  }, 
			   "1":{"Descripcion":"Agente Oficial",
					"Direccion":"Larrea 613",
					"HoradeTrabajo":"lu a sab 9 - 16hs",
					"Sucursal":"Agente Oficial",
					"Telefono1":"35314726"
				  },
				"2":{"Descripcion":"Sucursal",
					"Direccion":"Cerrito 408",
					"HoradeTrabajo":"lu a vi 11 - 20hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"3":{"Descripcion":"Sucursal",
					"Direccion":"Av. Pres. Roque Sáenz Peña 625",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"4":{"Descripcion":"Sucursal",
					"Direccion":"Virrey Cevallos 1641",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"5":{"Descripcion":"Sucursal",
					"Direccion":"Perú 1224",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"6":{"Descripcion":"Sucursal",
					"Direccion":"Av. Velez Sarsfield 1860",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"7":{"Descripcion":"Sucursal",
					"Direccion":"Juan B. Ambrosetti 768",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"8":{"Descripcion":"Sucursal",
					"Direccion":"Av. Santa Fe 2915",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"6":{"Descripcion":"Sucursal",
					"Direccion":"Godoy Cruz 2746",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"7":{"Descripcion":"Sucursal",
					"Direccion":"José Hernández 2379",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"8":{"Descripcion":"Sucursal",
					"Direccion":"Av. Rivadavia 10600",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"9":{"Descripcion":"Sucursal",
					"Direccion":"Juan Agustín García 3906",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"10":{"Descripcion":"Sucursal",
					"Direccion":"Av. Álvarez Thomas 1440",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"11":{"Descripcion":"Sucursal",
					"Direccion":"José Hernández 2379",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  }				  				  				  				  				        				  				    
				}';

				// armo un array 	
				//$branchOffices = json_decode($jsonResponse, true);
  			 	foreach ( json_decode($jsonResponse, true)  as $k => $value  ){
  			 		$Address = new Address($value['Direccion']);
  			 		$Bo = new BranchOffice($k, $value['Descripcion'], $Address, $value['HoradeTrabajo'], null, $value['Telefono1']);	
  			 		$branchOffices[] = $Bo;
  			 	}

	    }

	    return $branchOffices;  
	}

	/*
	*	$params $pack, $location
	*/
	public function getQuote( array $params=null)
	{
		$cuote = $this->execute("/sucursales.json", "get", $params);	    
	    return $cuote;
	}

	public function confirm($pack, $location, $address)
	{
		return 0;
	}



	public function execute($jsonUrl, $method, $params)
	{
		return 0;
	}

}

?>