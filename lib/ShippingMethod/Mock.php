<?php
namespace Omnipack\ShippingMethod;
use Omnipack\Data as Data;

//// require_once('Soap.php');
//include_once(__DIR__ .'/../OmniPackInterface.php');
//use OmniPack\OmniPackInterface as OmniPackInterface;

class Mock extends \Omnipack\Connector\Rest implements \Omnipack\iOmnipack
{
	protected $ws;

	public function __construct()
	{
		parent::__construct("endpoint");
	}

	public function setAutentication(array $params)
	{
		return true;
	}

	public function getBranchOffices(array $filters)
	{
		$branchOffices = array();
	    if ( is_array($filters) ){
	    	//$branchOffices = $this->execute("/sucursales.json","get",$filters);
	    	  	// simulo respuesta del servicio
	    	$jsonResponse = '{
	    		"0":{ "Descripcion":"Agente Oficial GARCÍA CLAUDIO",
					"Direccion":"Av. Piedra Buena 3660",
					"HoradeTrabajo":"lu a vi 9 - 18hs",
					"Sucursal":"Agente Oficial",
					"Telefono1":"4981-7449"
				  },
			   "1":{"Descripcion":"Agente Oficial",
					"Direccion":"Av. Rivadavia 1134",
					"HoradeTrabajo":"lu a sab 9 - 16hs",
					"Sucursal":"Agente Oficial",
					"Telefono1":"35314726"
				  },
				"2":{"Descripcion":"Sucursal",
					"Direccion":"Maipú 753",
					"HoradeTrabajo":"lu a vi 11 - 20hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  },
				"3":{"Descripcion":"Sucursal",
					"Direccion":"25 De Mayo 386",
					"HoradeTrabajo":"lu a vi 11 - 17hs",
					"Sucursal":"sucursal",
					"Telefono1":"45822392"
				  }
				}';

				// armo un array
				//$branchOffices = json_decode($jsonResponse, true);
  			 	foreach ( json_decode($jsonResponse, true)  as $k => $value  ){
								// $Address = new Address($value['Direccion']);
  			 		$Bo = new Data\Location\BranchOffice($k, $value['Descripcion'], $value['Direccion'], $value['HoradeTrabajo'], null, $value['Telefono1']);
  			 		$branchOffices[] = $Bo;
  			 	}
	    }

	    return $branchOffices;
	}

	/*
	*	$params $pack, $location
	*/
	public function getQuote(Data\Pack $pack, Data\Location\Location $location, $priority, $aditionalParams) {
		// $cuote = $this->execute("/sucursales.json", "get", $params);
			$quote = new \Omnipack\Data\Quote(15.25, 5, array('deadline_unit' => 'd&iacute;as h&aacute;biles'));
	    return $quote;
	}

	public function confirm(Data\Pack $pack, Data\Location\Location $location, Data\Addressee $addressee, $priority, $aditionalParams)
	{
		return true;
	}



	public function execute($jsonUrl, $method, $params)
	{
		return 0;
	}

}

?>
