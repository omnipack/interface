<?php

namespace Omnipack\ShippingMethod;

require_once __DIR__ . "/../Data/Location/BranchOffice.php";
require_once __DIR__ . "/../Data/Location/Address.php";
// require_once('wsConections.php');

class Andreani //implements \Omnipack\iOmniPack
{
	const WSDL = "https://sucursales.andreani.com/ws?wsdl";
	const ENDPOINT = "https://sucursales.andreani.com/ws";
	const HEADER_NAMESPACE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	const HEADER_NAME = "Security";


	protected $connector;


	public function __construct() {
			// parent::__construct(self::WSDL, self::ENDPOINT);
			$this->connector = new \Omnipack\Connector\Soap(self::WSDL, self::ENDPOINT);
  }

	public function setAutentication(Array $autentication_data)
	{
		$username_token = array('UsernameToken' => array(
			"Username" => $autentication_data["Username"],
			"Password" => $autentication_data["Password"]
		) );
		$json_token = '{"UsernameToken":{"Username":"'.$autentication_data['Username'].'", "Password":"'.$autentication_data["Password"].'"}}';
		$obj = json_decode($json_token, false);
		$this->connector->setSoapHeader(self::HEADER_NAMESPACE, self::HEADER_NAME, $obj, true);
	}

    /*
	*	@params
	*/
	public function getBranchOffices(array $params)
	{
		var_dump($params);
		$consulta = array();
		if (array_key_exists('city', $params)) {
			$consulta['Localidad'] = $params['city'];
		}
		if (array_key_exists('postal_code', $params)) {
			$consulta['CodigoPostal'] = $params['postal_code'];
		}
		if (array_key_exists('state', $params)) {
			$consulta['Provincia'] = $params['state'];
		}
		var_dump($consulta);
		$response = $this->connector->call('ConsultarSucursales', array(
			'consulta' => $consulta
		));

		$branchOfficesResponse = $response['ConsultarSucursalesResult']['ResultadoConsultarSucursales'];


		// print_r($branchOfficesResponse);
		$branchOffices = array();

				// armo un array
				//$branchOffices = json_decode($jsonResponse, true);
  			 	foreach ( $branchOfficesResponse  as $k => $value  ){
  			 		$bo = new \Omnipack\Data\Location\BranchOffice($value['Sucursal'], $value['Descripcion'], $value['Direccion'], $value['HoradeTrabajo'], null, $value['Telefono1']);
  			 		$branchOffices[] = $bo;
  			 	}

	    return $branchOffices;
	}

	/*
	*	@params
	*/
	public function getCuote($params)
	{
		$cuote = $this->execute($params);

	    return $cuote;
	}

	public function confirm($pack, $location, $address)
	{
		return 0;
	}



}

?>
