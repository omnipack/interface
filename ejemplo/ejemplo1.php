<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>OmniPack - Ejemplo de Implementacion</title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      #map-container { height: 300px }
    </style>

  </head>
  <body>
  	<?php 	
  			$branchOffices = array();
  			if (!empty($_POST['shipping-method'])  ) {
 				include 'process.php';	
 			}

  			$arr_shmethod = array(
  					"andreani" => "Andreani", 
					"oca" => "Oca"
				); 

  			$arr_cities = array(
  					"capital-federal"=>"Ciudad de Buenos Aires",
					"gran-buenos-aires"=>"GBA",
					"cordoba"=>"Cordoba",
					"rosario"=>"Rosario",
					"mendoza"=>"Mendoza"
  				);

	?> 


  	<div class="container jumbotron">
		<div class="form-group col-md-12">
			<h3>Omnipack - Ejemplo de implementacion</h3>
		</div>
		<form role="form" method="post" action="ejemplo1.php">
			  	<div class="form-group col-md-12">
					<div class="form-group col-md-6">
					<label for="form-shipping-method">Seleccione medio de envio</label>
					<select name="shipping-method" class="form-control">
						<?php 
							foreach ($arr_shmethod as $k => $value) {
								$selected = ($_POST['shipping-method'] == $k )? 'selected':''; 
								echo '<option value="'.$k.'"  '.$selected.'  >'.$value.'</option>';
							}
						?>
					</select>
					</div>
				</div>  
				<div class="form-group col-md-12">  
				  <div class="form-group col-md-6">
				    <label for="form-city">Elija Ciudad</label>
				    <select name="city" class="form-control">
				    	<?php 
				    		foreach ($arr_cities as $k => $value) {
				    			$selected = ($_POST['city'] == $k )? 'selected':''; 
				    			echo '<option value="'.$k.'"  '.$selected.'  >'.$value.'</option>';
				    		}
				    	?>
					</select>
				  </div>
			  	</div>

			  	<div class="form-group col-md-12">
			 	  <div class="form-group col-md-4">
					  	<button type="submit" class="btn  btn-success ">Ver Sucursales</button>	
				  </div>
				</div>

		</form>
		<div class="form-group col-md-12">
			<div id="map-container" class="form-group col-md-10"></div>
		</div>	
		    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

		    <!-- Include all compiled plugins (below), or include individual files as needed -->
		    <script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js"></script>
		    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		    <script >	

		    	function init_map() {
					
					var direcciones=new Array();

					<?php
						if ( !empty($branchOffices) && is_array($branchOffices) ) {
						    foreach ( $branchOffices as $k => $suc ) { 
						            echo 'direcciones['.$k.']="'. $suc->getAddress()->getStreet() .'";';
						    }
						}

					?>

					geocoder = new google.maps.Geocoder();

					for (var i = 0; i < direcciones.length; i++) {
						setDireccion(i);
					}

					function setDireccion(INDEX) {
						var request = new Object(); //CREO UN OBJETO
						request.address = direcciones[INDEX] + ", Ciudad de Buenos Aires"; //sé que son direcciones en capital
						geocoder.geocode(request, addAddressToMap); //geocode hace la conversión a un punto, y su segundo parámetro es una función de callback
					}

					function addAddressToMap(response, status) {
						if(!response) return;    //si no pudo
						//creo el marcador con la posición, el mapa, y el icono
						marker = new google.maps.Marker({
						'position': response[0].geometry.location,
						'map': map,
						'title':"sucursal"
						});
						marker.setMap(map); //inserto el marcador en el mapa
					}


					var var_mapoptions = {
					  center:  new google.maps.LatLng(-34.6097573,-58.4502459),
					  zoom: 12
					};
					var map = new google.maps.Map(document.getElementById("map-container"),
					    var_mapoptions);

				}

				google.maps.event.addDomListener(window, 'load', init_map);



			/*	 
				$(document).ready(function(){
		             //getdeails será nuestra función para enviar la solicitud ajax
		             

					$.get('process.php',
						  { 'shipping-method': 'andreani' },
						  function(datos) {
						   // alert(datos);
						      init_map(datos);
						      google.maps.event.addDomListener(window, 'load', init_map);

						  });


		        }); 				
			*/	 
		    </script>

	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
  </body>
</html>